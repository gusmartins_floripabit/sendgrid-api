import { sendEmail } from '../../lib/sendGrid'
import { kafkaConsumer } from '../kafka.consumer'

type DecodedKafkaMessage = {
  deliveryTo: string
  report: never
}

export async function createConsumer() {
  const consumer = await kafkaConsumer('SENDING_EMAIL')
  await consumer.run({
    eachMessage: async ({ message }) => {
      const messageToString = message.value!.toString()
      const { deliveryTo, report } = JSON.parse(
        messageToString,
      ) as DecodedKafkaMessage

      await sendEmail({ deliveryTo, report })
    },
  })
}

createConsumer()
