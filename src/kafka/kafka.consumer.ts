import { kafka } from '.'

export const kafkaConsumer = async (topic: string) => {
  const consumer = kafka.consumer({ groupId: 'REPORT' })
  await consumer.connect()
  await consumer.subscribe({
    topic,
    fromBeginning: false,
  })

  return consumer
}
