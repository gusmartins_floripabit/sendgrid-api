import { Kafka, logLevel } from 'kafkajs'

export const kafka = new Kafka({
  brokers: ['nearby-leopard-13166-us1-kafka.upstash.io:9092'],
  ssl: true,
  sasl: {
    mechanism: 'scram-sha-256',
    username: 'bmVhcmJ5LWxlb3BhcmQtMTMxNjYkquvXjvOHhHDsE1eeV8_17rxI09xpanP2vu8',
    password: 'MTM0ZWU5MGEtYjM0OS00ZDgwLWE4OTctMzA2ZDY2YzVjYWUz',
  },
  logLevel: logLevel.ERROR,
})
