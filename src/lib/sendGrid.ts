import mailService from '@sendgrid/mail'

import { env } from '../env'

mailService.setApiKey(env.SENDGRID_API_KEY)

async function sendEmail({
  deliveryTo,
  report,
}: {
  deliveryTo: string
  report: never
}) {
  try {
    await mailService.send({
      from: {
        email: env.EMAIL_FROM,
        name: env.NAME_EMAIL_FROM,
      },
      to: deliveryTo,
      attachments: [
        {
          filename: 'relatorio.xlsx',
          type: 'text/xlsx',
          content: Buffer.from(report, 'utf-8').toString('base64'),
        },
      ],
      templateId: env.SUCCESS_TEMPLATE_ID,
    })
  } catch (error) {
    await mailService.send({
      from: {
        email: env.EMAIL_FROM,
        name: env.NAME_EMAIL_FROM,
      },
      subject: 'Datalife - Solicitação de relatório',
      to: deliveryTo,
      templateId: env.FAILURE_TEMPLATE_ID,
    })
  }
}

export { sendEmail }
