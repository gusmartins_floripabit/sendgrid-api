import 'dotenv/config'

import { z } from 'zod'

const envSchema = z.object({
  NODE_ENV: z.enum(['dev', 'production']).default('dev'),
  PORT: z.coerce.number().default(5001),
  SENDGRID_API_KEY: z.string(),
  EMAIL_FROM: z.string().email(),
  NAME_EMAIL_FROM: z.string(),
  SUCCESS_TEMPLATE_ID: z.string(),
  FAILURE_TEMPLATE_ID: z.string(),
})

const _env = envSchema.safeParse(process.env)

if (_env.success === false) {
  console.error('❌ Invalid enviroment variables', _env.error.format())

  throw new Error('Invalid enviroment variables')
}

export const env = _env.data
